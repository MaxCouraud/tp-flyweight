<?php
/**
 * Created by PhpStorm.
 * User: COURAUD
 * Date: 11/04/2019
 * Time: 22:23
 */

namespace App;


class CaractereFactory
{
    private static $caractere = array();

    public static function creer($lettre, $police, $taille, $couleur)
    {
        $indice = $lettre . $police . $taille . $couleur;
        $caractere = new Caractere($lettre, $police, $taille, $couleur);
        self::$caractere[$indice] = $caractere;
        return $caractere;
    }
}