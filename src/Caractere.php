<?php
/**
 * Created by PhpStorm.
 * User: COURAUD
 * Date: 11/04/2019
 * Time: 22:07
 */

namespace App;


class Caractere implements ICaractere
{
    private $lettre;
    private $police;
    private $taille;
    private $couleur;

    public function __construct($lettre, $police, $taille, $couleur)
    {

        $this->lettre = $lettre;
        $this->police = $police;
        $this->taille = $taille;
        $this->couleur = $couleur;
    }


    public function afficher($positon, $isGras, $isItalic, $isSouligne, $isMajuscule)
    {
        echo $this->lettre;
    }

    /**
     * @return mixed
     */
    public function getLettre()
    {
        return $this->lettre;
    }

    /**
     * @return mixed
     */
    public function getPolice()
    {
        return $this->police;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @return mixed
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param mixed $lettre
     * @return Caractere
     */
    public function setLettre($lettre)
    {
        $this->lettre = $lettre;
        return $this;
    }

    /**
     * @param mixed $police
     * @return Caractere
     */
    public function setPolice($police)
    {
        $this->police = $police;
        return $this;
    }

    /**
     * @param mixed $taille
     * @return Caractere
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
        return $this;
    }

    /**
     * @param mixed $couleur
     * @return Caractere
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
        return $this;
    }

}