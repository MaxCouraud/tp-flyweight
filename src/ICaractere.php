<?php
/**
 * Created by PhpStorm.
 * User: COURAUD
 * Date: 11/04/2019
 * Time: 22:07
 */

namespace App;


interface ICaractere
{
    public function afficher($positon, $isGras, $isItalic, $isSouligne, $isMajuscule);
}